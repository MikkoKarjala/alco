import { Component, OnInit } from '@angular/core';
import { threadId } from 'worker_threads';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  private weight: number;
  private genders = [];
  private gender: string;
  private time = [];
  private aika: number;
  private bottles = [];
  private maara: number;

  private promilles: number;
  private liters: number;
  private grams: number;
  private gramsLeft: number;
  private burning: number;


  constructor() {}

  ngOnInit() {

    this.genders.push('mies');
    this.genders.push('nainen');

    this.time.push(1);
    this.time.push(2);
    this.time.push(3);
    this.time.push(4);
    this.time.push(5);
    this.time.push(6);
    this.time.push(7);
    this.time.push(8);
    this.time.push(9);
    this.time.push(10);

    this.bottles.push(1);
    this.bottles.push(2);
    this.bottles.push(3);
    this.bottles.push(4);
    this.bottles.push(5);
    this.bottles.push(6);
    this.bottles.push(7);
    this.bottles.push(8);
    this.bottles.push(9);
    this.bottles.push(10);

    this.liters = this.maara * 0.33;
    this.grams = this.maara * 8 * 4.5;
    this.burning = this.weight / 10;
    this.gramsLeft = this.grams - (this.burning * this.aika);
  }


  private calculate() {
    let factor = 0;

    switch (this.gender) {
      case 'mies':
        factor = 0.7;
        break;
      case 'nainen':
        factor = 0.6;
        break;
    }

    if (this.gender === 'mies') {
      this.promilles = (this.liters / this.aika * this.gramsLeft) * factor;
    }
    else {
      this.promilles = (this.liters / this.aika * this.gramsLeft) * factor;
    }
  }

}
